There are two ways to open a project:

# **From the Studio project dialog**

This is the screen that appears the first time that you open Android Studio. You will also see it if you close your current project(s).
* From the project dialog choose the second option: **Open an existing Android Studio Project**
![projectWizard.png](https://bitbucket.org/repo/AgGn79z/images/3896365451-projectWizard.png)

* In the file dialog that appears, navigate into the project directory of the project (the one that contains the app folder)

![fileOpenWizard.png](https://bitbucket.org/repo/AgGn79z/images/1325294790-fileOpenWizard.png)

* Click Choose

# **From the file menu** #

Use this method if you already have a project open.

* Choose File/Open from the menu

![fileOpen.png](https://bitbucket.org/repo/AgGn79z/images/78419784-fileOpen.png)

* In the file dialog that appears, navigate into the project directory of the project (the one that contains the app folder)

![fileOpenInProject.png](https://bitbucket.org/repo/AgGn79z/images/3791803850-fileOpenInProject.png)

* Click Choose


# Run Your App #

### Set up your device as follows: ###

1. Connect your device to your development machine with a USB cable. If you're developing on Windows, you might need to install the appropriate USB driver for your device. For help installing drivers, see the [OEM USB Drivers](https://developer.android.com/studio/run/oem-usb.html) document.
2. Enable **USB debugging** on your device by going to **Settings > Developer options**.
*Note: On Android 4.2 and newer, Developer options is hidden by default. To make it available, go to Settings > About phone and tap Build number seven times. Return to the previous screen to find Developer options.*

### Run the app from Android Studio as follows:###
1. In Android Studio, click the **app** module in the **Project** window and then select **Run > Run** (or click Run  in the toolbar).
2. In the **Select Deployment Target** window, select your device, and click **OK**.
Android Studio installs the app on your connected device and starts it.