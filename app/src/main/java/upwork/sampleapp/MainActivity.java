package upwork.sampleapp;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener {

    @BindView(R.id.iv_play)
    ImageView ivPlay;
    @BindView(R.id.edt_time)
    EditText edtTime;
    @BindView(R.id.tv_counter)
    TextView tvCounter;

    /**boolean to check if audio started*/
    boolean isStarted;

    /**mediaplayer*/
    private MediaPlayer mediaPlayer;

    /**time interval*/
    private int mInterval;

    /**handler for audio repeat*/
    private Handler mHandler;
    private Handler sHandler;

    private int time;
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        /** initialization of handlers */
        mHandler = new Handler();
        sHandler = new Handler();

        /**mediaPlayer initialize, set options and path to audio*/
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        try {
            mediaPlayer.setDataSource(getApplicationContext(),
                    Uri.parse("android.resource://upwork.sampleapp/raw/sound"));
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**initialize media session*/
        MediaSession.Callback callback = new MediaSession.Callback() {
            @Override
            public void onPlay() {
                onButtonClicked();
                if (!isStarted)
                    onButtonClicked();
            }
        };
        MediaSession mediaSession = new MediaSession(getApplicationContext(), "MainActivity"); // Debugging tag, any string
        mediaSession.setFlags(
                MediaSession.FLAG_HANDLES_MEDIA_BUTTONS |
                        MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setCallback(callback);

        /**Set up what actions you support and the state of your player*/
        mediaSession.setPlaybackState(
                new PlaybackState.Builder()
                        .setActions(PlaybackState.ACTION_PLAY |
                                PlaybackState.ACTION_PAUSE |
                                PlaybackState.ACTION_PLAY_PAUSE)
        .setState(PlaybackState.STATE_STOPPED,
                0, // playback position in milliseconds
                (float) 1.0). build());// playback speed

        /**Call this when you start playback after receiving audio focus*/
        mediaSession.setActive(true);
    }

    /**
     * Handle play button click.
     * Change image of the btn.
     * Start/stop playing audio
     */
    @OnClick(R.id.iv_play)
    public void onButtonClicked() {
        if (isStarted) {
            /**change the btn to play and stop playing audio*/
            ivPlay.setImageResource(R.drawable.play);
            stopRepeatingTask();
            stopTimer();

            isStarted = false;
        } else {
            /**check if the interval is set*/
            if (edtTime.getText().toString().isEmpty()) {
                Toast.makeText(getApplicationContext(), "Enter interval in seconds", Toast.LENGTH_SHORT).show();
                return;
            }

            startTimer();
            /**convert interval to milliseconds, change play btn to stop
             * and start playing audio repeatedly*/
            mInterval = Integer.parseInt(edtTime.getText().toString()) * 1000;
            ivPlay.setImageResource(R.drawable.stop);
            startRepeatingTask();

            isStarted = true;
        }
    }

    /**runnable which repeats audio after some interval*/
    private Runnable playRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                startAudio();
            } finally {
                mHandler.postDelayed(playRunnable, mInterval);
            }
        }
    };

    /**runnable to start the playRunnable after some interval*/
    private Runnable startRunnable = new Runnable() {
        @Override
        public void run() {
            playRunnable.run();
        }
    };

    /**start playRunnable after interval delay*/
    private void startRepeatingTask() {
        sHandler.postDelayed(startRunnable, mInterval);
    }

    /**stop repeating audio*/
    void stopRepeatingTask() {
        stopAudio();
        mHandler.removeCallbacks(playRunnable);
        sHandler.removeCallbacks(startRunnable);
    }

    /**start audio*/
    private void startAudio() {
        /**Set type to streaming and start*/
        mediaPlayer.start();
    }
    public void onPrepared(MediaPlayer player) {
        //mediaPlayer.start();
    }

    /**stop audio*/
    private void stopAudio() {
        /**check if mediaPlayer is null*/
        if (mediaPlayer != null) {
            try {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    mediaPlayer.seekTo(0);
                }
            } catch(IllegalStateException e){
                e.printStackTrace();
            }
        }
    }

    /** timer to show audio played time*/
    private void startTimer(){
        time = 0;
        timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        tvCounter.setText(String.valueOf(time++));
                    }
                });
            }
        };
        timer.scheduleAtFixedRate(task, 0, 1000);
    }

    /** stop timer*/
    private void stopTimer() {
        timer.cancel();
        timer.purge();
    }
}
